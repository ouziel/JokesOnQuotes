var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var htmlConfig = new HtmlWebpackPlugin({
	template: __dirname + '/src/index.html',
	filename: 'index.html',
	inject: 'body'
})
module.exports = {
	entry: './src/index.js',
	output: {
		path: __dirname + '/dist',
		filename: 'bundle.js'
	},
	devServer: {
		historyApiFallback: true,
		port: 4100,
		stats: {
			colors: true,
			hash: false,
			version: false,
			timings: false,
			assets: false,
			chunks: false,
			modules: false,
			reasons: false,
			children: false,
			source: false,
			errors: true,
			errorDetails: false,
			warnings: false,
			publicPath: true
		}
	},
	module: {
		loaders: [
			{
				test: /\.(js|jsx)$/,
				loader: 'babel-loader',
				exclude: /node_modules/,
				query: {
					presets: ['es2015', 'react']
				}
			}, {
				test: /\.css$/,
				loader: 'style-loader!css-loader'
			}, {
				test: /\.scss$/,
				loaders: ['style', 'css', 'sass']
			}, {
				test: /\.(png|jpg|jpeg|gif|woff)$/,
				loader: 'url-loader?limit=8192'
			}, {
				test: /\.(eot|svg|ttf|woff|woff2)$/,
				loader: 'file?name=/fonts/[name].[ext]'
			}
		]
	},
	plugins: [htmlConfig],
	resolve: {
		root: path.resolve(__dirname),
		extensions: [
			'', '.js', '.jsx'
		],
		alias: {
			ui: __dirname + '/src/scripts/components/ui/',
			config: __dirname + '/src/scripts/config/',
			actions: __dirname + '/src/scripts/actions/',
			components: __dirname + '/src/scripts/components/',
			containers: __dirname + '/src/scripts/containers/'
		}
	},
	modulesDirectories: ['node_modules']
}