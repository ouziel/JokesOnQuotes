import {createStore, applyMiddleware, compose} from 'redux';
import {syncHistoryWithStore} from 'react-router-redux';
import {browserHistory} from 'react-router';
import rootReducer from './scripts/reducers';
import axios from 'axios';
import axiosMiddleware, {multiClientMiddleware} from 'redux-axios-middleware';
import thunk from 'redux-thunk';
import config from 'config';
const clients = {
	default: {
		client: axios.create({baseURL: config.baseURL, responseType: 'json'})
	},
	googleSearch: {
		client: axios.create({baseURL: 'https://www.googleapis.com/customsearch/v1', responseType: 'json'})
	}
}
const enhancers = compose(applyMiddleware(thunk, multiClientMiddleware(clients)), window.devToolsExtension
	? window.devToolsExtension()
	: f => f);
if (module.hot) {
	module
		.hot
		.accept('./reducers/', () => {
			const nextRootReducer = require('./reducers/index').default;
			store.replaceReducer(nextRootReducer);
		});
}
let store = createStore(rootReducer, {}, enhancers)
export const history = syncHistoryWithStore(browserHistory, store);
export default store;