import store from './store';
function firebaseInit() {
	// FIREBASE CONFIG
	const firebaseConfig = {
		apiKey: "AIzaSyDncbxySz_DaQ7RT9ohKuqJVshCWsBiPBs",
		authDomain: "ezavealaza.firebaseapp.com",
		databaseURL: "https://ezavealaza.firebaseio.com",
		storageBucket: "ezavealaza.appspot.com"
	};
	firebase.initializeApp(firebaseConfig);
	// firebase listenr for auth change
	////////////////
	firebase
		.auth()
		.onAuthStateChanged(function(user) {
			if (user) {
				// User is signed in.
				var displayName = user.displayName;
				var email = user.email;
				var emailVerified = user.emailVerified;
				var photoURL = user.photoURL;
				var isAnonymous = user.isAnonymous;
				var uid = user.uid;
				var refreshToken = user.refreshToken;
				var providerData = user.providerData;
				store.dispatch({type: 'LOGGED_IN', user})
			} else {
				// User is signed out.
				store.dispatch({type: 'LOGGED_OUT'})
			}
		});
}
export default firebaseInit;