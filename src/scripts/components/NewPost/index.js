import React, {PropTypes} from 'react';
import TextField from 'ui/TextField';
import Chip from 'ui/Chip';
import {browserHistory} from 'react-router';
export default class NewPost extends React.Component {
	constructor(props) {
		super(props);
		// 1) set initial state to be the store's state, override on user input
		const {
			Auth: {
				Authenticated,
				user: {
					displayName,
					photoURL
				}
			},
			Posts: {
				editor: {
					quote,
					quoted,
					isEditing,
					key
				}
			}
		} = this.props;
		const figureImg = photoURL;
		const figureName = displayName;
		const figureText = figureName;
		this.state = {
			quote,
			quoted,
			isEditing,
			key,
			figureImg,
			figureName,
			figureText
		}
	}
	sendForm(e) {
		e.preventDefault();
		const {isEditing} = this.state, {Auth, addNewPost, addNewJoke} = this.props;
		const displayName = Auth.user.displayName,
			uid = Auth.user.uid;
		let mainJoke,
			joke,
			quote,
			quoted,
			quotedImg;
		const figureImg = this.state.figureImg;
		const figureName = this.state.figureText || displayName;
		if (isEditing) {
			// add mainJoke
			joke = this.refs.mainJoke.value;
			// const key = this.state.key;
			// addNewJoke(mainJoke, key)
			addNewJoke({joke, figureImg, figureName})
		} else {
			// add quote + optional mainJoke
			quote = this.refs.quote.value,
			quoted = this.state.quotedText,
			quotedImg = this.state.quotedImg,
			mainJoke = this.refs.mainJoke.value;
			addNewPost({
				quote,
				quoted,
				quotedImg,
				displayName,
				uid,
				mainJoke,
				figureName,
				figureImg
			});
		}
	}
	onChange(value) {
		const key = Object.keys(value);
		this.setState({[key]: value[key]})
	}
	changeQuote() {
		const quote = this.refs.quote.value;
		this.setState({quote});
	}
	renderTextQuote() {
		const {
			editor: {
				key,
				quote,
				quoted
			}
		} = this.props.Posts;
		return (
			<div className="post" data-key={key}>
				<h1>{quote}</h1>
				<h6>{quoted}</h6>
			</div>
		)
	}
	renderInputQuote() {
		const {editor: {
				isEditing
			}} = this.props.Posts;
		return (
			<div className="form_row">
				<input type="text" ref="quote" onChange={this
					.changeQuote
					.bind(this)}/>
				<Chip search={this.props.search} disabled={isEditing
					? true
					: false} name="quoted" text={this.state.quotedText} change={this
					.onChange
					.bind(this)}/>
			</div>
		);
	}
	render() {
		const {editor: {
				isEditing
			}} = this.props.Posts;
		return (
			<section className="container">
				<form onSubmit={this
					.sendForm
					.bind(this)}>
					<div className="form_row">
						<div className="subheader">עצה</div>
					</div>
					{isEditing
						? this.renderTextQuote()
						: this.renderInputQuote()}
					<div className="form_row">
						<div className="subheader">הלצה</div>
					</div>
					<div className="form_row">
						<Chip search={this.props.search} disabled={false} name="figure" img={this.state.figureImg} text={this.state.figureText} change={this
							.onChange
							.bind(this)}/>
						<input type="text" ref="mainJoke"/>
					</div>
					<div className="form_row">
						<input type="submit" value="שליחה"/>
					</div>
				</form>
			</section>
		);
	}
}
NewPost.propTypes = {};