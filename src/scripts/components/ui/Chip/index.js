import React, {PropTypes} from 'react';
import colors from 'config/colors';
import TetherComponent from 'react-tether'
import defaultStyles from './styles';
export default class Chip extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			text: '',
			isOpen: false,
			data: [],
			img: ''
		}
	}
	componentDidMount() {
		const {text, disabled, img} = this.props;
		if (disabled) {
			const text = this.props.text;
			this.refs.text.value = text;
		} else {
			this.setState({text});
		}
		this.setState({img})
	}
	componentWillReceiveProps(nextProps) {
		const {text, img} = nextProps;
		if (img) {
			this.setState({img})
		}
		this.setState({text});
		// const text = this.props.text;
		// this.refs.text.value = text;
	}
	toggleBox() {
		const {disabled} = this.props;
		if (!disabled) {
			this.setState({
				isOpen: !this.state.isOpen
			})
			const {text} = this.state;
			if (text) {
				const getData = this
					.props
					.search(text)
					.then(data => this.setState({data}))
			}
		}
	}
	changeImage(img) {
		this.setState({img})
		this.toggleBox();
		let state = {
			[`${this.props.name}Img`]: img
		}
		this
			.props
			.change(state);
	}
	renderImgBox() {
		const {data} = this.state;
		console.log(this);
		return (
			<div className="images_wrapper">
				<ul>
					{(data.length)
						? data.map(item => {
							return (
								<li><img src={item} onClick={this
									.changeImage
									.bind(this, item)}/></li>
							)
						})
						: null}
				</ul>
			</div>
		)
	}
	changeChipText(e) {
		const text = this.refs.text.value;
		this.setState({text})
		let state = {
			[`${this.props.name}Text`]: text
		}
		this
			.props
			.change(state);
	}
	render() {
		const {color, inline, disabled} = this.props;
		const {img} = this.state;
		const styles = {
			chip: {
				backgroundColor: color,
				display: (inline)
					? 'inline-flex'
					: 'flex'
			},
			chipText: {
				display: (disabled)
					? 'flex'
					: 'none'
			},
			img: {
				backgroundImage: `url(${img})`
			},
			input: {
				display: (disabled)
					? 'none'
					: 'block'
			}
		};
		return (
			<div className="chip" style={styles.chip}>
				<TetherComponent attachment="bottom center" constraints={[{
						to: 'scrollParent',
						attachment: 'together'
					}
				]}>
					{/* quoted figure img */}
					<div className="chip_img" style={styles.img} onClick={this
						.toggleBox
						.bind(this)}></div>
					{/* tooltip box */}
					{this.state.isOpen
						? this.renderImgBox()
						: null}
				</TetherComponent>
				<div className="chip_text" style={styles.text}>
					<input ref="text" type="text" style={styles.input} value={this.state.text} onChange={this
						.changeChipText
						.bind(this)}/>
					<span style={styles.chipText}>{this.props.text}</span>
				</div>
			</div>
		);
	}
}
Chip.defaultProps = {
	img: '',
	text: '',
	disabled: true,
	color: colors.getColor('0')
}