const styles = {
	chip: {
		display: 'flex',
		justifyContent: 'space-between',
		height: 35,
		borderRadius: 15,
		alignItems: 'center'
	},
	chipText: {
		borderRadius: 15,
		position: 'relative',
		top: 2,
		paddingRight: 5,
		paddingLeft: 15,
		alignItems: 'center',
		justifyContent: 'center',
		lineHeight: '35px'
	},
	img: {
		backgroundSize: 'cover',
		backgroundPosition: '50%',
		width: 35,
		height: 35,
		backgroundColor: 'blue',
		borderRadius: '50%'
	},
	text: {
		flex: 1,
		borderRadius: 15
	},
	input: {
		border: 0,
		backgroundColor: 'transparent',
		height: 30,
		outline: 0,
		fontSize: 16,
		textIndent: 5
	}
}
export default styles;