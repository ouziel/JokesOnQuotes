import React, {PropTypes} from 'react';
class TextField extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		const props = this.props;
		return (<input className="input" {...props}/>);
	}
}
TextField.defaultProps = {
	value: '',
	type: 'text',
	hint: ''
};
TextField.propTypes = {};
export default TextField;