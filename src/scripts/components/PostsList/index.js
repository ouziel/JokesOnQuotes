import React, {PropTypes} from 'react';
import {browserHistory} from 'react-router'
import Navbar from '../Nav/Nav.js';
import Jokes from './Jokes.js';
import Chip from 'ui/Chip';
export default class PostsList extends React.Component {
	constructor(props) {
		super(props);
	}
	changePage(id) {
		browserHistory.push({pathname: '/posts', query: {
				id
			}});
	}
	render() {
		const jokes = this.props.Jokes;
		// get the queried post
		const id = parseInt(this.props.routing.locationBeforeTransitions.query.id) || 0;
		const item = this.props.Posts.data[id];
		return (
			<div className="container">
				<section className="foo">
					{item
						? <Posts jokes={jokes[item.key]} item={item}/>
						: 'no posts'}
				</section>
			</div>
		);
	}
}
const Posts = ({item, jokes}) => {
	console.log(item);
	return (
		<div className="page">
			<div className="post" data-key={item.key}>
				<h1>{item.post.quote}</h1>
				<Chip inline disabled text={item.post.quoted} img={item.post.quotedImg}/>
			</div>
			<div className="jokes">
				<Jokes jokes={jokes} id={item.key}/>
			</div>
		</div>
	)
}