import React, {PropTypes} from 'react';
import Chip from 'ui/Chip';
const styles = {
	chip: {
		margin: 4
	}
};
export default class Jokes extends React.Component {
	constructor(props) {
		super(props);
	}
	componentDidMount() {}
	render() {
		const {jokes} = this.props;
		return (
			<ul>
				{jokes
					? Object
						.keys(jokes)
						.map((key, i) => {
							return (<Joke key={i} info={jokes[key]}/>)
						})
					: null}
			</ul>
		);
	}
}
const Joke = ({info}) => {
	return (
		<li>
			<Chip img={info.figureData.figureImg} text={info.figureData.figureName} color={info.figureData.chipColor}/>
			<span>{info.joke}</span>
		</li>
	)
}
Jokes.propTypes = {};