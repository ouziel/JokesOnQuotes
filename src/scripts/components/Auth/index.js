import React, {PropTypes} from 'react';
import styles from './style.scss';
const Auth = (props) => {
	return (
		<section className="container">
			<h1 className="login_title">התחברות
			</h1>
			<ul className="login_providers">
				<li className="login_provider">
					<button className="facebook_btn" onClick={props.facebookLogin}>facebook</button>
				</li>
			</ul>
		</section>
	);
}
Auth.propTypes = {};
export default Auth;