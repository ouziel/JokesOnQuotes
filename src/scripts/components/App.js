import React, {PropTypes} from 'react';
import Navbar from './Nav/Nav.js';
import Nav from './Nav/'; // weird, it doesnt look for Nav/index.js
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import Symbols from './Symbols';
export default class App extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		return (
			<div>
				<Symbols/>
				<Nav/>
				<ReactCSSTransitionGroup component="div" transitionName="fade" transitionEnterTimeout={500} transitionLeaveTimeout={500}>
					{React.cloneElement(this.props.children, Object.assign({}, this.props, {key: this.props.location.pathname}))}
				</ReactCSSTransitionGroup>
				<Navbar {...this.props}/>
			</div>
		);
	}
}