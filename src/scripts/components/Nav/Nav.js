import React, {PropTypes} from 'react';
import {Link, browserHistory} from 'react-router';
const Symbol = ({
	id,
	cls = 'icon'
}) => {
	return (
		<svg className={cls}>
			<use href={`#${id}`}/>
		</svg>
	)
}
export default class Navbar extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		return (
			<nav className="navbar">
				<button className="button next-prev prev" onClick={this.props.prevPost}>
					<Symbol id="arrow_right"/>
				</button>
				<button className="button next-prev next" onClick={this.props.nextPost}>
					<Symbol id="arrow_left"/>
				</button>
				<Link to="/whichPost" onClick={(e) => this.props.validate(e)}>
					<button className="button add">
						<Symbol id="plus"/>
					</button>
				</Link>
			</nav>
		);
	}
}
Navbar.propTypes = {};