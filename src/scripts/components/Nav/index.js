import React, {PropTypes} from 'react';
import {Link} from 'react-router';
import NavToggler from './navToggler';
import Logo from '../Logo';
export default class Nav extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			scale: 1,
			open: false
		}
	}
	toggleNav() {
		const {open} = this.state;
		if (open) {
			this.setState({scale: 1, open: false})
		} else {
			this.setState({scale: 25, open: true})
		}
	}
	closeNav() {
		this.toggleNav();
	}
	render() {
		return (
			<nav className="main_navigation">
				<div className="toggler_box">
					<NavToggler scale={this.state.scale} clickHandler={this
						.toggleNav
						.bind(this)}/>
					<Logo/>
				</div>
				<ul className={this.state.open
					? 'list_on'
					: 'list '}>
					<li>
						<Link to="/" onClick={this
							.closeNav
							.bind(this)}>ציטוטים</Link>
					</li>
					<li>
						<Link to="/addPost" onClick={this
							.closeNav
							.bind(this)}>חדש</Link>
					</li>
				</ul>
			</nav>
		);
	}
}
Nav.propTypes = {};