import React, {PropTypes} from 'react';
const NavToggler = (props) => {
	return (
		<div onClick={props.clickHandler}>
			<svg viewBox="0 0 100 100" className="toggleMenu">
				<circle cx="50%" cy="50%" r="20" fill="blue" style={{
					transform: `scale(${props.scale})`
				}}/>
				<rect x="25%" y="40%" width="25%" height="5%"></rect>
				<rect x="25%" y="50%" width="25%" height="5%"></rect>
				<rect x="25%" y="60%" width="25%" height="5%"></rect>
			</svg>
		</div>
	);
}
NavToggler.propTypes = {};
export default NavToggler;