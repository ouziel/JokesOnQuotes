import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {facebookLogin} from '../../actions/Auth';
import Auth from '../../components/Auth';
const mapStateToProps = (state, ownProps) => {
	return state;
};
function mapDispatchToProps(dispatch) {
	return bindActionCreators({
		facebookLogin
	}, dispatch)
}
const AuthContainer = connect(mapStateToProps, mapDispatchToProps)(Auth);
export default AuthContainer;