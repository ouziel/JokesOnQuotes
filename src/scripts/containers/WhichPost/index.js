import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {newQuoteAndJoke, newQuote} from '../../actions/Nav';
import WhichPost from '../../components/NewPost/which.js';
const mapStateToProps = (state, ownProps) => {
	return state;
};
function mapDispatchToProps(dispatch) {
	return bindActionCreators({
		newQuoteAndJoke,
		newQuote
	}, dispatch)
}
const WhichPostContainer = connect(mapStateToProps, mapDispatchToProps)(WhichPost);
export default WhichPostContainer;