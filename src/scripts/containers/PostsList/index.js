import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {getPosts} from '../../actions/Posts';
import {changePost, nextPost, prevPost} from '../../actions/Nav';
import PostsList from '../../components/PostsList';
const mapStateToProps = (state, ownProps) => {
	return state;
};
function mapDispatchToProps(dispatch) {
	return bindActionCreators({
		getPosts,
		nextPost,
		prevPost,
		changePost
	}, dispatch)
}
const PostsListContainer = connect(mapStateToProps, mapDispatchToProps)(PostsList);
export default PostsListContainer;