import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {addNewPost, addNewJoke} from '../../actions/Posts';
import {search} from 'actions/search';
import NewPost from '../../components/NewPost';
const mapStateToProps = (state, ownProps) => {
	return state;
};
function mapDispatchToProps(dispatch) {
	return bindActionCreators({
		addNewPost,
		addNewJoke,
		search
	}, dispatch)
}
const PostsContainer = connect(mapStateToProps, mapDispatchToProps)(NewPost);
export default PostsContainer;