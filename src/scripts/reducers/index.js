import {combineReducers} from 'redux'
import {firebaseStateReducer} from 'redux-react-firebase'
import {routerReducer} from 'react-router-redux';
import Auth from './Auth/index.js'
import Jokes from './Jokes/index.js'
import Posts from './Posts/index.js'
// const rootReducer = combineReducers({text, Auth, firebase: firebaseStateReducer, routing: routerReducer})
const rootReducer = combineReducers({Auth, Jokes, Posts, routing: routerReducer})
export default rootReducer