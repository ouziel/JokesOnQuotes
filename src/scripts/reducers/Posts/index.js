import firebase from 'firebase';
const initialState = {
	data: [],
	currentPostID: 0,
	editor: {
		isEditing: false,
		quote: '',
		quoted: '',
		key: ''
	}
};
const PostsReducer = (state = initialState, action) => {
	switch (action.type) {
		case 'INCOMING_POSTS':
			return Object.assign({}, state, {
				data: [
					...state.data, {
						post: action.payload.val,
						key: action.payload.key
					}
				]
			});
		case 'NEW_QUOTE':
			return Object.assign({}, state, {editor: initialState.editor});
		case 'NEW_QUOTE_AND_JOKE':
			return Object.assign({}, state, {
				editor: {
					isEditing: true,
					quote: action.post.quote,
					quoted: action.post.quoted,
					key: action.key
				}
			});
		case 'CHANGE_POST':
			const currentPostID = action.id;
			return Object.assign({}, state, {currentPostID});
		default:
			return state;
	}
}
export default PostsReducer;