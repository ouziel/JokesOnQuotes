import firebase from 'firebase';
const fakeAuth = {
	Authenticated: true,
	user: {
		displayName: 'Or Ouziel',
		photoURL: "https://scontent.xx.fbcdn.net/v/t1.0-1/p100x100/10304971_10205361483976537_689666399737414079_n.jpg?oh=d8c56fb21c9deaa7c391dd7846327f81&oe=5833785E",
		uid: 'BWrm4tXYoIZ4xB09S7oIfNv5eXH3',
		email: 'ouzielor@gmail.com',
		provider: 'facebook.com'
	}
}
const emptyAuth = {
	Authenticated: false,
	user: {
		displayName: '',
		photoURL: "",
		uid: '',
		email: '',
		provider: ''
	}
}
const initialState = fakeAuth
const AuthReducer = (state = initialState, action) => {
	switch (action.type) {
		case 'USER_LOGIN_REDIRECT':
			return state;
		case 'LOGGED_OUT':
			return state;
		case 'LOGGED_IN':
			const user = {
				email: action.user.email,
				displayName: action.user.displayName,
				uid: action.user.uid,
				photoURL: action.user.photoURL,
				provider: action.user.providerData[0].providerId
			}
			return Object.assign({}, state, {
				Authenticated: true,
				user
			});
		default:
			return state;
	}
}
export default AuthReducer;