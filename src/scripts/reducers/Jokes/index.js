const initialState = {};
const JokesReducer = (state = initialState, action) => {
	switch (action.type) {
		case 'INCOMING_JOKES':
			return Object.assign({}, state, {
				[action.payload.key]: action.payload.val
			});
		default:
			return state;
	}
}
export default JokesReducer;