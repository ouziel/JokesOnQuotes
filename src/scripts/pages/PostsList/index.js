import React, {PropTypes} from 'react';
import PostsListContainer from '../../containers/PostsList';
const PostsListPage = (props) => {
	return (
		<div>
			<PostsListContainer/>
		</div>
	);
}
export default PostsListPage;