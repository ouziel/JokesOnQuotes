import {browserHistory} from 'react-router';
const validate = (e) => {
	e.preventDefault();
	return (dispatch, getState) => {
		dispatch({type: 'VALIDATE_USER_IS_AUTHED'})
		const {Auth: {
				Authenticated
			}} = getState();
		if (!Authenticated) {
			dispatch({type: 'FORCE_AUTH'})
			browserHistory.push({pathname: '/auth'});
		} else {
			dispatch({type: 'PICK_POST_TYPE'})
			browserHistory.push({pathname: '/whichPost'});
		}
	}
}
export default validate;