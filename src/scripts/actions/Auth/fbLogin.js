import firebase from 'firebase';
const facebookLogin = () => {
	return (dispatch) => {
		if (!firebase.auth().currentUser) {
			dispatch({type: 'INIT_FB_LOGIN'});
			const provider = new firebase
				.auth
				.FacebookAuthProvider();
			firebase
				.auth()
				.signInWithPopup(provider)
				.then(result => {
					const token = result.credential.accessToken,
						user = result.user;
					dispatch({type: 'LOGGED_IN', user});
				})
				.catch(error => {
					const errorCode = error.code,
						errorMessage = error.message,
						email = error.email,
						credential = error.credential;
					if (errorCode === 'auth/account-exists-with-different-credential') {
						alert('You have already signed up with a different auth provider for that email.');
					} else {
						console.error(error);
					}
				});
		} else {
			dispatch({type: 'SINGING_OUT'});
			firebase
				.auth()
				.signOut();
		}
	}
}
export default facebookLogin