import {facebookLogin, validate} from './Auth/index.js'
import {addNewPost} from './Posts/index.js';
import {changePost, prevPost, nextPost} from './Nav/index.js';
export {
	addNewPost,
	changePost,
	prevPost,
	nextPost,
	validate,
	facebookLogin
}