import {browserHistory} from 'react-router'
function changePost(dir) {
	return (dispatch, getState) => {
		let id;
		const {
			Posts: {
				data,
				currentPostID
			}
		} = getState();
		if (dir === 'next') {
			id = currentPostID + 1;
		} else if (dir === 'prev') {
			id = currentPostID - 1;
		}
		if (id > data.length - 1) {
			id = 0;
		}
		if (id < 0) {
			id = data.length - 1;
		}
		dispatch({type: 'CHANGE_POST', id})
		browserHistory.push({pathname: '/posts', query: {
				id
			}});
	}
}
function nextPost() {
	return (dispatch) => {
		dispatch(changePost('next'))
	}
}
function prevPost() {
	return (dispatch) => {
		dispatch(changePost('prev'))
	}
}
function newQuote() {
	return (dispatch) => {
		const type = 'QUOTE'
		dispatch({type: 'NEW_QUOTE'});
		browserHistory.push({pathname: '/addPost'});
	}
}
function newQuoteAndJoke() {
	return (dispatch, getState) => {
		const {Posts, Posts: {
				currentPostID
			}} = getState();
		const {post} = Posts.data[currentPostID];
		const key = Posts.data[currentPostID].key;
		dispatch({type: 'NEW_QUOTE_AND_JOKE', post, key})
		browserHistory.push({pathname: '/addPost'});
	}
}
function addJoke(joke) {
	return (dispatch, getState) => {
		console.log('foo');
	}
}
export {changePost, newQuoteAndJoke, newQuote, prevPost, nextPost};