const config = {
	key: 'AIzaSyDncbxySz_DaQ7RT9ohKuqJVshCWsBiPBs',
	url: 'https://www.googleapis.com/customsearch/v1',
	type: 'cx',
	id: '011819547800723665017:hafagc4lt4s'
}
// const getResults = (response) => {
// 	return (dispatch) => {
// 		const payload = response.payload,
// 			data = payload.data,
// 			items = data.items;
// 		let results = [];
// 		const maxResults = 100;
// 		for (let i = 0; i < maxResults; i++) {
// 			const item = items[i];
// 			try {
// 				const img = item.pagemap.cse_thumbnail[0].src;
// 				results.push(img);
// 			} catch (error) {
// 				continue;
// 			}
// 		}
// 		return results;
// 	}
// }
const getResults = (response) => {
	return (dispatch) => {
		const payload = response.payload,
			data = payload.data,
			items = data.items;
		let results = [];
		const maxResults = 100;
		for (let i = 0; i < maxResults; i++) {
			const item = items[i];
			try {
				const img = item.pagemap.cse_thumbnail[0].src;
				results.push(img);
			} catch (error) {
				continue;
			}
		}
		return results;
	}
}
const search = (query) => {
	return (dispatch, getState) => {
		const {type, id, key} = config,
			url = `?key=${key}&${type}=${id}&q=${query}`;
		return dispatch({
				type: 'SEARCH_IMAGES',
				payload: {
					client: 'googleSearch',
					request: {
						url
					}
				}
			})
			.then(response => dispatch(getResults(response)))
			.catch(error => console.log({error}))
	}
}
export default search;