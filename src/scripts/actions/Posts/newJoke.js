import firebase from 'firebase';
import colors from 'config/colors';
function addNewJoke({joke, figureName, figureImg}) {
	const chipColor = colors.getColor();
	return (dispatch, getState) => {
		const {
			Posts: {
				editor: {
					key
				}
			},
			Auth: {
				user: {
					uid,
					displayName,
					photoURL
				}
			}
		} = getState();
		const figureData = {
			figureName,
			figureImg,
			chipColor
		}
		const jokeData = {
			displayName,
			uid,
			joke,
			photoURL,
			figureData
		}
		const newJokeKey = firebase
			.database()
			.ref()
			.child('posts-jokes/' + key)
			.push()
			.key;
		const updates = {};
		updates[`/posts-jokes/${key}/${newJokeKey}`] = jokeData;
		firebase
			.database()
			.ref()
			.update(updates)
			.then(() => dispatch({type: 'ADD_JOKE', jokeData}))
	}
}
export default addNewJoke;