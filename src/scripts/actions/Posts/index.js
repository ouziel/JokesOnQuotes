import addNewPost from './add'
import {getPosts, getJokes} from './get'
import addNewJoke from './newJoke'
export {addNewPost, addNewJoke, getJokes, getPosts}