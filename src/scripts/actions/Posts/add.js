import firebase from 'firebase';
import {browserHistory} from 'react-router'
import colors from 'config/colors';
import {changePost} from '../Nav';
function goToNewPost() {
	return (dispatch, getState) => {
		const {Posts: {
				data
			}} = getState();
		const id = data.length - 1;
		browserHistory.push({pathname: '/posts', query: {
				id
			}});
		dispatch({type: 'GOTO_NEW_POST'});
	}
}
export default function addNewPost({
	uid,
	displayName,
	quote,
	quoted,
	quotedImg,
	mainJoke,
	figureName,
	figureImg
}) {
	return function(dispatch, getState) {
		const chipColor = colors.getColor();
		const figureData = {
			figureName,
			figureImg,
			chipColor
		}
		const jokeData = {
			displayName,
			uid,
			joke: mainJoke,
			figureData
		}
		const quoteData = {
			displayName,
			uid,
			mainJoke,
			quote,
			quoted,
			quotedImg,
			starCount: 0
		};
		/// POSTS KEYS
		const newQuoteKey = firebase
			.database()
			.ref()
			.child('posts')
			.push()
			.key;
		const newJokeKey = firebase
			.database()
			.ref()
			.child('posts-jokes/' + newQuoteKey)
			.push()
			.key;
		/// UPDATE
		const updates = {};
		updates[`/posts-jokes/${newQuoteKey}/${newJokeKey}`] = jokeData;
		updates['/posts/' + newQuoteKey] = quoteData;
		updates['/user-posts/' + uid + '/' + newQuoteKey] = {
			quoteData,
			jokeData
		};
		firebase
			.database()
			.ref()
			.update(updates)
			.then(() => {
				dispatch({type: 'ADD_QUOTE'})
				dispatch({type: 'ADD_JOKE', jokeData});
				dispatch(goToNewPost())
			});
	}
}