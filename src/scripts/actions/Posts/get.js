import firebase from 'firebase';
import config from 'config';
function getPosts() {
	return (dispatch) => {
		const postsRef = firebase
			.database()
			.ref('posts')
			.limitToLast(config.firebase.quotesLimit);
		postsRef.on('child_added', (snapshot) => {
			const key = snapshot.key,
				val = snapshot.val();
			dispatch({
				type: 'INCOMING_POSTS',
				payload: {
					key,
					val
				}
			});
		});
	}
}
function getJokes() {
	return (dispatch) => {
		const jokesRef = firebase
			.database()
			.ref('posts-jokes')
		jokesRef.on('child_added', (snapshot) => {
			const key = snapshot.key,
				val = snapshot.val();
			dispatch({
				type: 'INCOMING_JOKES',
				payload: {
					key,
					val
				}
			});
		});
	}
}
export {getPosts, getJokes}