const colors = {
	data: [
		'#bcaaa4',
		'#ce93d8',
		'#b39ddb',
		'#9fa8da',
		'#90caf9',
		'#80deea',
		'#ffab91',
		'#ffcc80',
		'#fff59d',
		'#e6ee9c',
		'#c5e1a5',
		'#a5d6a7',
		'#80cbc4',
		'#ef9a9a'
	],
	getColor(index) {
		// random or index
		return (index)
			? this.data[parseInt(index)]
			: this.data[Math.floor(Math.random() * (this.data.length - 1))];
	}
}
export default colors;
// '#BDDDF4',
// '#E1F3F7',
// '#D1C3E0',
// '#DCECC8',
// '#e6ee9c',
// '#B3E0DB',
// '#ffccbc',
// '#e1bee7',