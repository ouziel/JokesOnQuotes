import colors from './colors';
import firebase from './firebase';
const baseURL = 'http://localhost:4100/';
const config = {
	firebase,
	colors
}
export {firebase, colors}
export default config;