import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import validate from 'actions/Auth/validate';
import * as actionCreators from './actions';
console.log('offff', actionCreators);
/*
	Main View
*/
import App from './components/App';
/*
make the new state avaiable via props
*/
function mapStateToProps(state) {
	return {state};
}
/*
	make the actions available via props
*/
export function mapDispatchToProps(dispatch) {
	return bindActionCreators(actionCreators, dispatch);
}
var AppContainer = connect(mapStateToProps, mapDispatchToProps)(App);
export default AppContainer;