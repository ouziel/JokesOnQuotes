//////////////////////////////
///          REACT CORE
/////////////////////////////
import React, {PropTypes} from 'react'
import ReactDOM from 'react-dom'
import {browserHistory, Router, Route, IndexRoute, Link} from 'react-router'
import config from 'config';
//////////////////////////////
///          APP CORE
/////////////////////////////
import AppContainer from './scripts/AppContainer'
import './styles/style.scss';
//////////////////////////////
///          PAGES
/////////////////////////////
import AuthPage from './scripts/pages/Auth'
import AddPostPage from './scripts/pages/AddPost'
import PostsPage from './scripts/pages/PostsList'
import WhichPostPage from './scripts/pages/WhichPost'
//////////////////////////////
///          STORE
/////////////////////////////
import {Provider} from 'react-redux'
import store, {history} from './store';
//////////////////////////////
///          INIT
/////////////////////////////
import firebaseInit from './firebase';
import {getPosts, getJokes} from './scripts/actions/Posts/get';
//////////////////////////////
///          APP TREE
/////////////////////////////
ReactDOM.render(
	<Provider store={store}>
	<Router history={history}>
		<Route path="/" component={AppContainer}>
			<IndexRoute component={PostsPage}/>
			<Route path="/auth" component={AuthPage}/>
			<Route path="/whichPost" component={WhichPostPage}/>
			<Route path="/addPost" component={AddPostPage} onEnter= { (a, replace) => { const {Auth: { Authenticated }} = store.getState(); if (!Authenticated) { replace('/auth'); } } }>
				<Route path="/addPost/:type" component={AddPostPage}/>
			</Route>
			<Route path="/posts" component={PostsPage}>
				<Route path="/posts/:id" component={PostsPage}/>
			</Route>
		</Route>
	</Router>
</Provider>, document.getElementById('root'));
/// INIT FIREBASE LINK
firebaseInit();
// GER POSTS
store.dispatch(getPosts());
// GET JOKES
store.dispatch(getJokes());